# UNI #


Monolithic banks and financial institutions cater to corporations and wealthy businesses rather than typical people.
The goal of UNI is to provide financial empowerment and autonomy to individuals.

UNI is a personal decentralized financial banking system for the individual.
Instead of putting your money in a central institutionalized bank, 
your money exists as a secure versatile digital asset that only you can control.

A UNI account consists of
(1) a digital wallet that holds conventional currency [USD] and stable cryptocurrencies like Bitcoin and
(2) a mobile / web app that produces a virtual card capable of debit card financial transactions.